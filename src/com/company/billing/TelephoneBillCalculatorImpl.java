package com.company.billing;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;

import static java.lang.Long.max;
import static java.lang.Math.min;

public class TelephoneBillCalculatorImpl implements TelephoneBillCalculator {
    @Override
    public BigDecimal calculate(String phoneLog) {
        Map<String, ClientInfo> info = new HashMap<>();
        List<String> phoneLogs = Arrays.asList(phoneLog.split("\n"));
        phoneLogs.forEach((log) -> {
            String phoneNumber = parseNumberFromLine(log);
            BigDecimal price = calculateOne(log);
            if(info.containsKey(phoneNumber)){
                ClientInfo last = info.get(phoneNumber);
                info.put(phoneNumber, new ClientInfo(phoneNumber, price.add(last.price), last.calls + 1));
            } else {
                info.put(phoneNumber, new ClientInfo(phoneNumber, price, 1));
            }
        });
        BigDecimal finalPrice = BigDecimal.ZERO;
        ClientInfo clientInfoWithMaximumCalls = new ClientInfo();
        for (ClientInfo value : info.values()) {
            finalPrice = finalPrice.add(value.price);
            if (value.calls > clientInfoWithMaximumCalls.calls){
                clientInfoWithMaximumCalls = value;
            } else if (value.calls == clientInfoWithMaximumCalls.calls){
                if (Long.parseLong(value.phoneNumber) >  Long.parseLong(clientInfoWithMaximumCalls.phoneNumber)){
                    clientInfoWithMaximumCalls = value;
                }
            }
        }
        return finalPrice.subtract(clientInfoWithMaximumCalls.price);

    }

    public BigDecimal calculateOne(String phoneLog){
        int startDateTimeFirstIndex = 13;
        int formattedDateTimeLength = 19;
        LocalDateTime begin = dateTime(parseFirstFormattedDate(phoneLog, startDateTimeFirstIndex, formattedDateTimeLength));
        LocalDateTime until = dateTime(parseFirstFormattedDate(phoneLog, startDateTimeFirstIndex + formattedDateTimeLength + 1, formattedDateTimeLength));
        long duration = duration(begin, until);
        long durationOfExpensiveCall = durationOfExpensiveCall(begin, until);
        return getPrice(duration, durationOfExpensiveCall);
    }

    private BigDecimal getPrice(long duration, long durationOfExpensiveCall){
        long durationOfCheapCall = max(durationOfExpensiveCall - duration, 0);
        if(duration > 5){
            BigDecimal priceWithoutDiscount = BigDecimal.valueOf(min(durationOfCheapCall, 5) * 0.5 + min(durationOfExpensiveCall, 5));
            return priceWithoutDiscount.add(BigDecimal.valueOf(duration-5).multiply(BigDecimal.valueOf(0.2)));
        } else {
            return BigDecimal.valueOf(durationOfExpensiveCall + durationOfCheapCall);
        }
    }

    private long duration(LocalDateTime from, LocalDateTime until) {
        return ChronoUnit.MINUTES.between(from, until);
    }

    private long durationOfExpensiveCall(LocalDateTime from, LocalDateTime until) {
        LocalDateTime endTimeOfExpensiveCall = from.withHour(16).withMinute(0);
        if(until.isBefore(endTimeOfExpensiveCall)){
            endTimeOfExpensiveCall = until;
        }
        return max(0, ChronoUnit.MINUTES.between(endTimeOfExpensiveCall, endTimeOfExpensiveCall));
    }

    private String parseNumberFromLine(String line) {
        return line.substring(0, 12);
    }

    private String parseFirstFormattedDate(String line, int startIndex, int length) {
        return line.substring(startIndex, startIndex + length);
    }

    private LocalDateTime dateTime(String formattedDateTime) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        return LocalDateTime.parse(formattedDateTime, formatter);
    }

    class ClientInfo{
        private BigDecimal price;
        private String phoneNumber;
        private long calls;
        ClientInfo(String phoneNumber, BigDecimal price, long calls){
            this.phoneNumber = phoneNumber;
            this.price = price;
            this.calls = calls;
        }
        ClientInfo(){
            this(null, BigDecimal.ZERO, 0);
        }

        @Override
        public String toString() {
            return "ClientInfo{" +
                    "price=" + price +
                    ", phoneNumber='" + phoneNumber + '\'' +
                    ", calls=" + calls +
                    '}';
        }
    }
}
