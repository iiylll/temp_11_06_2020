package com.company;

import com.company.billing.TelephoneBillCalculator;
import com.company.billing.TelephoneBillCalculatorImpl;

import java.math.BigDecimal;

public class Main {

    public static void main(String[] args) {
        TelephoneBillCalculator calc = new TelephoneBillCalculatorImpl();
        //TEST, TODO if needed add reading from the file
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("420774567453 13-01-2020 18:10:15 13-01-2020 18:12:57\n");
        stringBuilder.append("420774567453 13-01-2020 18:10:15 13-01-2020 19:12:57\n");
        stringBuilder.append("420774567452 13-01-2020 18:10:15 13-01-2020 22:12:57\n");
        BigDecimal price = calc.calculate(stringBuilder.toString());
        System.out.println(price);

    }
}
